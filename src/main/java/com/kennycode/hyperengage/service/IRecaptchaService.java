package com.kennycode.hyperengage.service;

import java.util.Locale;

import com.kennycode.hyperengage.model.UserMessage;

public interface IRecaptchaService {

	public boolean isTokenValid(String token);
	public UserMessage process(String token, String ip, Locale locale);
}
